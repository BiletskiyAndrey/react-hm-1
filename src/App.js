import React, { Component } from 'react';
import JsonDate from './jsonData.json';

const getData = {
    url: "http://bitrealt.com.ua/wp-json/wp/v2/house",
    init(surl) {
        "use strict";
        let url = surl || this.url;
        return fetch(url)
            .then(result => {
                return result.json();
            })
            .then(res => {
                return res;
            })
    }



};

console.log(getData.init());

class ListItem extends Component {
    constructor(props){
        super(props);
        this.state = {
            isDone: false
        };
    }
    handleClick = (event) => {
        let done = !this.state.isDone;
        this.setState({isDone: done})
    };
  render(){
      let {name} = this.props.item;
      const done = this.state.isDone;
      return(

          <div className="myList__Item">
              <span className={done ? "done" : ""}>{name}</span>
              <button onClick={this.handleClick}>done</button>
          </div>
      )
  }
}


class App extends Component {
  constructor(props){
    super(props);
    this.state = {
      contact: JsonDate,
      // newCont: this.contact
    };
  }
  handleChange = (event) => {
    let searchQuery = event.target.value.toLowerCase();
    let renderContacts = JsonDate.filter((item)=>{
      let itemName = item.name.toLowerCase();
      return itemName.indexOf(searchQuery)!== -1;
    });

    this.setState({contact:renderContacts});
  };

  render() {
    let {contact} = this.state;
    console.log('json',JsonDate);
    return (
      <div className="App">
        {/*<span>{newCont}</span>*/}
        Hello Bejika
        <input onChange={this.handleChange} />
          {
              contact.map((item, key) => {
              return(
                <ListItem
                    key={key}
                    item={item}
                />
          )})
          }
      </div>
    );
  }
}

export default App;
